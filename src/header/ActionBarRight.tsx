import React from 'react';
import { Avatar, Title, Caption, Menu } from 'react-native-paper'

import { View, Image, TouchableOpacity } from 'react-native';
import Images from '../utils/images'



const ActionBarImage = () => {
  
  return (
    <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity>
         <Avatar.Image size={45} source={Images.avatar_logo} />
         </TouchableOpacity>
         <TouchableOpacity>
         <Avatar.Image size={45} source={Images.shopping_logo} style={{backgroundColor: '#fff',marginLeft:5, marginRight:6}} />
         </TouchableOpacity>
    </View>
  );
};

export default ActionBarImage;