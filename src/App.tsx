//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native'
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { Login } from './modules/screens/loginScreen';
import splashScreen from './splash/screen';
import { Registration } from './modules/screens/registerScreen';
import ActionBarImage from './header/ActionBarImage';
import { Profile } from './modules/screens/profileScreen';
import { Home } from './modules/screens/homeScreen';
import ActionBarRight from './header/ActionBarRight'
import ActionIconLeft from './header/ActionIconLeft'

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


function getHeaderTitle(route) {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "Feed" as that's the first screen inside the navigator
    const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home';

    switch (routeName) {
        case 'Home':
            return 'Home';
        case 'Profile':
            return 'My profile';

    }
}

function home() {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Home" component={Home} />
            <Drawer.Screen name="Profile" component={Profile}  />
        </Drawer.Navigator>
    );
}


class App extends Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name='Splash' component={splashScreen} options={{ headerShown: false }} />
                    <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
                    <Stack.Screen name='Registration' component={Registration}
                        options={{ headerLeft: () => <ActionBarImage />, headerTitleStyle: { textAlign: 'center' } }} />
                    <Stack.Screen name='Home' component={home}
                        options={
                            ({ route }) => ({ headerTitle: getHeaderTitle(route), headerRight: ()=> <ActionBarRight/>
                            ,headerLeft:()=> <ActionIconLeft/> ,headerTitleStyle: { textAlign: 'center' } })} />

                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

//make this component available to the app
export default App;
