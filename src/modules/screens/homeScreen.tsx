//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Avatar, Caption, Title, Button } from 'react-native-paper'
import LinearGradient from 'react-native-linear-gradient'
import Images from '../../utils/images'
import color from '../../utils/color'
import styles from '../styles/homeStyle'

// create a component
class homeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>

                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', borderRadius: 10, marginHorizontal: 10 }}>
                        <View style={{ height: 100, width: 100 }}>
                            <Image source={Images.first_category} style={{ height: 80, width: 80, marginTop: 10, marginLeft: 10 }} />
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Ayurvedic</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', borderRadius: 10, marginHorizontal: 10, borderTopWidth: 1 }}>
                        <View style={{ height: 100, width: 100 }}>
                            <Image source={Images.second_category} style={{ height: 80, width: 80, marginTop: 10, marginLeft: 10 }} />
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Tablets</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', borderRadius: 10, marginHorizontal: 10, borderTopWidth: 1 }}>
                        <View style={{ height: 100, width: 100 }}>
                            <Image source={Images.third_category} style={{ height: 80, width: 80, marginTop: 10, marginLeft: 10 }} />
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>General Medicines</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', borderRadius: 10, marginHorizontal: 10, borderTopWidth: 1 }}>
                        <View style={{ height: 100, width: 100 }}>
                            <Image source={Images.fourth_category} style={{ height: 80, width: 80, marginTop: 10, marginLeft: 10 }} />
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Homopethic</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', borderRadius: 10, marginHorizontal: 10, borderTopWidth: 1 }}>
                        <View style={{ height: 100, width: 100 }}>
                            <Image source={Images.fifth_category} style={{ height: 80, width: 80, marginTop: 10, marginLeft: 10 }} />
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Vitamins</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={{ flexDirection: 'row', borderRadius: 10, marginHorizontal: 10, borderTopWidth: 1 }}>
                        <View style={{ height: 100, width: 100 }}>
                            <Image source={Images.sixth_category} style={{ height: 80, width: 80, marginTop: 10, marginLeft: 10 }} />
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, marginTop: 10, marginLeft: 20 }}>Paracetamol</Text>
                        </View>
                    </View>
                </TouchableOpacity>


            </View>
        );
    }
}

// define your styles

//make this component available to the app
export const Home = homeScreen;
