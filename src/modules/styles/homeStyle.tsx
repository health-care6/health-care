import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        // marginTop:10,
        backgroundColor: "#fff",
    },
    mainPic: {
        height: 250,
        marginHorizontal: 10
    },
    gradient: {
        flexDirection: 'row',
        borderRadius: 15,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    first_text: {
        alignSelf: 'flex-end',
        fontSize: 22,
        fontWeight: 'bold',
        color: '#fff',
        paddingLeft: 140,
        paddingBottom: 10,
    },
    category: {
        marginTop: 10,
        marginLeft: -6,
    },
    icon: {
        marginLeft: 15

    }
});
